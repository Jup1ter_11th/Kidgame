using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MainMenuController : MonoBehaviour
{

    void Start()
    {
        AudioManager.instance.PlayTrack(FilePath.resources_music + "BGM", volumeCap: 0.1f);

        StartCoroutine(StartScene());
    }

    public IEnumerator StartScene()
    {
        GraphicPanel black = GraphicPanelManager.instance.GetPanel("Transition");
        GraphicLayer blackLayer = black.GetLayer(0, true);

        yield return blackLayer.SetTexture(FilePath.resources_backgroundImages + "Black", immediate: true);

        yield return blackLayer.Clear(transitionspeed: 1f);

        AudioManager.instance.PlayVoice(FilePath.resources_voices + "Gamemenu");
    }


    public void loadButton(string nextScene)
    {
        AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Click", voloume: 0.5f);
        AudioManager.instance.StopSoundEffect("Gamemenu");
        GameManager.ResetScore();
        StartCoroutine(GraphicPanelManager.instance.TransitionScene(nextScene));
    }

    public void Exit()
    {
        Application.Quit();
    }
}