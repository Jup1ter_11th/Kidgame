using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // The script that manages all others
    public static GameManager instance = null;
    
    /// <summary>
    /// Description:
    /// Standard Unity function called when the script is loaded, called before start
    /// 
    /// When this component is first added or activated, setup the global reference
    /// Inputs: 
    /// none
    /// Returns: 
    /// void (no return)
    /// </summary>
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }
    
    [Header("Scores")]
    // The current player score in the game
    [Tooltip("The player's score")]
    [SerializeField] private int gameManagerScore = 0;
    
    [SerializeField] private float gameManagerTime = 0.0f;

    // Static getter/setter for player score (for convenience)
    public static int score
    {
        get
        {
            return instance.gameManagerScore;
        }
        set
        {
            instance.gameManagerScore = value;
        }
    }
    
    public static float TimeInSecond
    {
        get
        {
            return instance.gameManagerTime;
        }
        set
        {
            instance.gameManagerTime = value;
        }
    }

    public bool isWin { get; private set; } = false;
    public bool isLoss { get; private set; } = false;

    private static int sfxint = 0;

    /// <summary>
    /// Description:
    /// Standard Unity function called once before the first Update
    /// Inputs: 
    /// none
    /// Returns: 
    /// void (no return)
    /// </summary>
    private void Start()
    {
        HandleStartUp();
    }
    
    void HandleStartUp()
    {
        if (PlayerPrefs.HasKey("score"))
        {
            score = PlayerPrefs.GetInt("score");
        }
        if (PlayerPrefs.HasKey("Time In Second"))
        {
            TimeInSecond = PlayerPrefs.GetInt("Time In Second");
        }
    }
    
    /// <summary>
    /// Description:
    /// Adds a number to the player's score stored in the gameManager
    /// Input: 
    /// int scoreAmount
    /// Returns: 
    /// void (no return)
    /// </summary>
    /// <param name="scoreAmount">The amount to add to the score</param>
    public static void AddScore(int scoreAmount)
    {
        score += scoreAmount;
        PlayerPrefs.SetInt("score", score);
    }
    
    public static void AddTime(int timeAmount)
    {
        TimeInSecond += timeAmount;
    }
    
    /// <summary>
    /// Description:
    /// Resets the current player score
    /// Inputs: 
    /// none
    /// Returns: 
    /// void (no return)
    /// </summary>
    public static void ResetScore()
    {
        PlayerPrefs.SetInt("score", 0);
        score = 0;
    }
    
    public static void UpdateTime()
    {
        TimeInSecond += Time.deltaTime;
        PlayerPrefs.SetFloat("Time In Second", TimeInSecond);
    }
    
    
    public static void ResetTime()
    {
        PlayerPrefs.SetInt("Time In Second", 0);
    }

    public void Loadlevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void Win()
    {
        isWin = true;
        Time.timeScale = 0;
        AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Mission Clear 1");
        UIManager.instance.GoToPage(UIManager.instance.gameWinPageIndex);
    }
    
    public void Loss()
    {
        isLoss = true;
        Time.timeScale = 0;


        UIManager.instance.GoToPage(UIManager.instance.gameLossPageIndex);
    }

    public void OnApplicationQuit()
    {
        ResetScore();
    }

    public void ToggleTime()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
        }
    }

    public void CorrectSFX()
    {
        if (sfxint == 0)
        {
            AudioManager.instance.PlayVoice(FilePath.resources_sfx + "���ҡ���");
        }
        if (sfxint == 1)
        {
            AudioManager.instance.PlayVoice(FilePath.resources_sfx + "�١��ͧ���");
        }
        if (sfxint == 2)
        {
            AudioManager.instance.PlayVoice(FilePath.resources_sfx + "�������ҡ���");
        }

        sfxint++;

        if (sfxint >= 3)
        {
            sfxint = 0;
        }
    }

    public void IncorrectSFX()
    {
        if (sfxint == 0)
        {
            AudioManager.instance.PlayVoice(FilePath.resources_sfx + "��������ա�Դ�Ф�");
        }
        if (sfxint == 1)
        {
            AudioManager.instance.PlayVoice(FilePath.resources_sfx + "�ͧ�ա�������¤�");
        }
        if (sfxint == 2)
        {
            AudioManager.instance.PlayVoice(FilePath.resources_sfx + "�����������Ǥ��");
        }

        sfxint++;

        if (sfxint >= 3)
        {
            sfxint = 0;
        }
    }
}


