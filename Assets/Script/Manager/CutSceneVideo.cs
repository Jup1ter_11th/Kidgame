using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class CutSceneVideo : MonoBehaviour
{
    public VideoClip clipboy;
    public VideoClip clipgirl;
    private VideoClip clip;
    public string nextScene;

    void Start()
    {
        StartCoroutine(StartScene());

        StartCoroutine(NextScene());
    }

    void Update()
    {
        
    }

    private IEnumerator StartScene()
    {
        GraphicPanel black = GraphicPanelManager.instance.GetPanel("Transition");
        GraphicLayer blackLayer = black.GetLayer(0, true);

        GraphicPanel background = GraphicPanelManager.instance.GetPanel("Background");
        GraphicLayer backgroundLayer = background.GetLayer(0, true);

        yield return blackLayer.SetTexture(FilePath.resources_backgroundImages + "Black", immediate: true);

        yield return blackLayer.Clear(transitionspeed: 1f);

        if (PlayerPrefs.GetString("gender") == "boy")
        {
            clip = clipboy;
        }

        if (PlayerPrefs.GetString("gender") == "girl")
        {
            clip = clipgirl;
        }

        backgroundLayer.SetVideo(clip, loop: false);

        
    }

    private IEnumerator NextScene()
    {
        yield return new WaitForSeconds((float)clipboy.length + 0.5f);

        yield return GraphicPanelManager.instance.TransitionScene(nextScene);
    }
}
