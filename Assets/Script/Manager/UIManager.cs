using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager instance = null;
        
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    public List<GameObject> pages;
    
    public int gamePausePageIndex = 0;
    public int gameWinPageIndex = 0;
    public int gameLossPageIndex = 0;
    public int gameOptionPageIndex = 0;
        
    private int pageIndex;
    
    public bool allowPause = true;

    // Whether or not the application is paused
    private bool isPaused = false;

    public void SetActiveAllPages(bool activated)
    {
        if (pages != null)
        {
            foreach (GameObject page in pages)
            {
                if (page != null)
                    page.gameObject.SetActive(activated);
            }
        }
    }

    /// <summary>
    /// Description:
    /// If the game is paused, unpauses the game.
    /// If the game is not paused, pauses the game.
    /// Inputs:
    /// None
    /// Retuns:
    /// void (no return)
    /// </summary>
    public void TogglePause()
    {
        if (allowPause)
        {
            AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Click", voloume: 0.2f);

            if (isPaused)
            {
                SetActiveAllPages(false);
                Time.timeScale = 1;
                isPaused = false;
            }
            else
            {
                GoToPage(gamePausePageIndex);
                Time.timeScale = 0;
                isPaused = true;
            }
        }      
    }
    
    public void GoToPage(int pageIndex)
    {
        if (pageIndex < pages.Count && pages[pageIndex] != null)
        {
            SetActiveAllPages(false);
            pages[pageIndex].gameObject.SetActive(true);
        }
    }
}