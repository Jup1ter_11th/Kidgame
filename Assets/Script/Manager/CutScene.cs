using DIALOGUE;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutScene : MonoBehaviour
{
    [SerializeField] private TextAsset fileName;

    public Texture2D bgboy;
    public Texture2D bggirl;
    public AudioClip voice;
    public string nextScene;

    void Start()
    {
        
        StartCoroutine(StartScene());

        StartCoroutine(NextScene());
    }

    void Update()
    {
        
    }

    private IEnumerator StartScene()
    {
        GraphicPanel black = GraphicPanelManager.instance.GetPanel("Transition");
        GraphicLayer blackLayer = black.GetLayer(0, true);

        GraphicPanel background = GraphicPanelManager.instance.GetPanel("Background");
        GraphicLayer backgroundLayer = background.GetLayer(0, true);

        if (PlayerPrefs.GetString("gender") == "boy")
        {
            backgroundLayer.SetTexture(bgboy);
        }
            
        if (PlayerPrefs.GetString("gender") == "girl")
        {
            backgroundLayer.SetTexture(bggirl);
        }

        yield return blackLayer.SetTexture(FilePath.resources_backgroundImages + "Black", immediate: true);

        yield return blackLayer.Clear(transitionspeed: 1f);

        StartConversation();

        AudioManager.instance.PlayVoice(voice);

        if (fileName.name == "CutScene1")
            yield return CutScene1();

        if (fileName.name == "CutScene2")
            yield return CutScene2();

        if (fileName.name == "CutScene4")
            yield return CutScene4();

        if (fileName.name == "CutScene5")
            yield return CutScene5();

    }

    void StartConversation()
    {
        List<string> lines = FileManager.ReadTextAsset(fileName, true);

        DialogueSystem.instance.Say(lines);

    }

    private IEnumerator NextScene()
    {
        yield return new WaitForSeconds(voice.length + 1);

        yield return GraphicPanelManager.instance.TransitionScene(nextScene);
    }

    private IEnumerator CutScene1()
    {
        yield return new WaitForSeconds(15);

        DialogueSystem.instance.OnUserPrompt_Next();

        yield return new WaitForSeconds(10);

        DialogueSystem.instance.OnUserPrompt_Next();
    }

    private IEnumerator CutScene2()
    {
        yield return new WaitForSeconds(8);

        DialogueSystem.instance.OnUserPrompt_Next();

        yield return new WaitForSeconds(7);

        DialogueSystem.instance.OnUserPrompt_Next();

    }

    private IEnumerator CutScene4()
    {
        yield return new WaitForSeconds(9);

        DialogueSystem.instance.OnUserPrompt_Next();

    }

    private IEnumerator CutScene5()
    {
        yield return new WaitForSeconds(7);

        DialogueSystem.instance.OnUserPrompt_Next();

        yield return new WaitForSeconds(8);

        DialogueSystem.instance.OnUserPrompt_Next();

        yield return new WaitForSeconds(10);

        DialogueSystem.instance.OnUserPrompt_Next();

        yield return new WaitForSeconds(12);

        DialogueSystem.instance.OnUserPrompt_Next();

        yield return new WaitForSeconds(8);

        DialogueSystem.instance.OnUserPrompt_Next();

    }
}
