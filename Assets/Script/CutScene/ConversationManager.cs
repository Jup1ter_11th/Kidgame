using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DIALOGUE
{
    public class ConversationManager
    {
        private DialogueSystem dialogueSystem => DialogueSystem.instance;

        private Coroutine process = null;
        public bool isRunning => process != null;

        private TextArchitect architect = null;
        private bool userPrompt = false;

        public ConversationManager(TextArchitect architect)
        {
            this.architect = architect;
            dialogueSystem.onUserPrompt_Next += OnUserPrompt_Next;
        }

        private void OnUserPrompt_Next()
        {
            userPrompt = true;
        }

        public Coroutine StartConversation(List<string> conversation)
        {
            dialogueSystem.dialogueComplete = false;
            
            StopConversation();

            process = dialogueSystem.StartCoroutine(RunningConersation(conversation));

            return process;
        }

        public void StopConversation()
        {
            if (!isRunning)
            {
                return;
            }
        }

        IEnumerator RunningConersation(List<string> conversation)
        {
            for (int i = 0; i < conversation.Count; i++)
            {
                //Don't show any blank lines or try to run any logic on them
                if (string.IsNullOrWhiteSpace(conversation[i]))
                    continue;

                Dialogue_Line line = DialogueParser.Parse(conversation[i]);

                //show dialogue
                if (line.hasDialogue)
                {
                    yield return Line_RunDialogue(line);
                }

                //Run any commands
                if (line.hasCommand)
                {
                    yield return Line_RunCommand(line);
                }

                //Wait for user input if dialgue was in this line
                if (line.hasDialogue)
                {
                    //Wait for user input
                    yield return WaitForUserInput();

                    CommandManager.instance.StopAllProcess();
                }
                    
            }

            dialogueSystem.dialogueComplete = true;

        }

        IEnumerator Line_RunDialogue(Dialogue_Line line)
        {
            if (line.hasSpeaker)
            {
                HandleSpeakerLogic(line.speakerData);
            }    

            //Build dialogue
            yield return BuildLineSegment(line.dialogueData);

        }

        private void HandleSpeakerLogic(DL_Speaker_Data speakerData)
        {
            bool characterMustBeCreated = (speakerData.makeCharacterEnter || speakerData.isCastingExpression || speakerData.isCastingPosition);

            Character character = CharacterManager.instance.GetCharacter(speakerData.name, createIfDoesNotExist: characterMustBeCreated);

            if (speakerData.makeCharacterEnter && (!character.IsVisible && !character.isRevealing))
                character.Show();

            //Add Character name to the UI
            dialogueSystem.ShowSpeakerName(speakerData.displayname);

            //Now customize the dialogue for this character - if applicable
            //DialogueSystem.instance.ApplySpeakerDataToDialogueContainer(speakerData.name);

            //Cast Expression
            if (speakerData.isCastingPosition)
                character.MoveToPosition(speakerData.castPosition);

            //Cast Expression
            if (speakerData.isCastingExpression)
            {
                foreach (var ce in speakerData.CastExpressions)
                    character.OnReceiveCastingExpression(ce.layer, ce.expression);
            }

        }

        IEnumerator Line_RunCommand(Dialogue_Line line)
        {
            List<DL_Command_Data.Command> commands = line.commandData.commands;

            foreach(DL_Command_Data.Command command in commands)
            {
                if (command.waitForCompletion || command.name == "wait")
                {
                    CoroutineWrapper cw = CommandManager.instance.Execute(command.name, command.aruguments);
                    while(!cw.isDone)
                    {
                        if(userPrompt)
                        {
                            CommandManager.instance.StopCurrentProcess();
                            userPrompt = false;
                        }
                        yield return null;
                    }
                }
                    
                else
                    CommandManager.instance.Execute(command.name, command.aruguments);
            }

            yield return null;
        }

        IEnumerator BuildLineSegment(DL_Dialogue_Data line)
        {
            for(int i = 0; i < line.segments.Count; i++)
            {
                DL_Dialogue_Data.Dialoge_Segment segment = line.segments[i];

                yield return WaitForDialogueSegmentSignalToBeTriggered(segment);

                yield return BuildDialogue(segment.dialogue, segment.appendText);
            }
        }

        IEnumerator WaitForDialogueSegmentSignalToBeTriggered(DL_Dialogue_Data.Dialoge_Segment segment)
        {
            switch(segment.startSignal) 
            {
                case DL_Dialogue_Data.Dialoge_Segment.StartSignal.C:
                case DL_Dialogue_Data.Dialoge_Segment.StartSignal.A:
                    yield return WaitForUserInput();
                    break;
                case DL_Dialogue_Data.Dialoge_Segment.StartSignal.WC:
                case DL_Dialogue_Data.Dialoge_Segment.StartSignal.WA:
                    yield return new WaitForSeconds(segment.signalDelay);
                    break;
                default: 
                    break;
            }
        }

        IEnumerator BuildDialogue(string dialogue, bool append = false)
        {
            //Build the dialogue
            if (!append)
                architect.Build(dialogue);
            else
                architect.Append(dialogue);

            //Wait for the dialogue to complete
            while (architect.isBuilding)
            {
                if (userPrompt == true)
                {
                    if (!architect.hurryUp)
                        architect.hurryUp = true;
                    else
                        architect.ForceComplete();

                    userPrompt = false;
                }
                yield return null;
            }
        }

        IEnumerator WaitForUserInput()
        {
            //dialogueSystem.prompt.Show();
            
            while(!userPrompt)
                yield return null;
            
            //dialogueSystem.prompt.Hide();

            userPrompt = false;
        }

    }
}