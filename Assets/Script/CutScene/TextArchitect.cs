using System.Collections;
using UnityEngine;
using TMPro;

public class TextArchitect : MonoBehaviour
{
    private TextMeshProUGUI tmproUI;
    private TextMeshPro tmproWorld;
    public TMP_Text tmpro => tmproUI != null ? tmproUI : tmproWorld;

    public string currentText => tmpro.text;
    public string targetText { get; private set; } = "";
    public string preText { get; private set; } = "";

    public string fullTargetText => preText + targetText;

    public enum BuildMethod { instant, typwriter, fade}
    public BuildMethod buildMethod = BuildMethod.typwriter;

    public Color textColor 
    { 
        get 
        { 
            return tmpro.color; 
        } 
        set 
        { 
            tmpro.color = value; 
        } 
    }

    public float speed 
    { 
        get 
        { 
            return baseSpeed * speedMultiplier; 
        } 
        set
        {
            speedMultiplier = value;
        }
    }
    private const float baseSpeed = 0.25f;
    private float speedMultiplier = 1;

    public int characterPerCycle
    {
        get
        {
            return speed <= 2f ? characterMultipiler : speed <= 2.5f ? characterMultipiler * 2 : characterMultipiler * 3;
        }
    }
    private int characterMultipiler = 1;

    public bool hurryUp = false;

    public TextArchitect(TextMeshProUGUI tmproUI)
    {
        this.tmproUI = tmproUI;
    }

    public TextArchitect(TextMeshPro tmproWorld)
    {
        this.tmproWorld = tmproWorld;
    }

    public Coroutine Build(string text)
    {
        preText = "";
        targetText = text;

        Stop();

        buildProcess = tmpro.StartCoroutine(Building());
        return buildProcess;
    }

    public Coroutine Append(string text)
    {
        preText = tmpro.text;
        targetText = text;

        Stop();

        buildProcess = tmpro.StartCoroutine(Building());
        return buildProcess;
    }

    private Coroutine buildProcess = null;
    public bool isBuilding => buildProcess != null;

    public void Stop()
    {
        if (!isBuilding)
            return;

        tmpro.StopCoroutine(buildProcess);
        buildProcess = null;
    }

    IEnumerator Building()
    {
        Prepare();

        switch(buildMethod)
        {
            case BuildMethod.typwriter:
                yield return Build_Typewriter();
                break;
            case BuildMethod.fade:
                yield return Build_Fade();
                break;
        }

        OnComplete();
    }

    public void ForceComplete()
    {
        switch(buildMethod)
        { 
            case BuildMethod.typwriter:
                tmpro.maxVisibleCharacters = tmpro.textInfo.characterCount;
                break;
            case BuildMethod.fade:
                break;
        }
    }

    private void OnComplete()
    {
        buildProcess = null;
        hurryUp = false;
    }

    private void Prepare()
    {
        switch(buildMethod)
        {
            case BuildMethod.instant:
                Prepare_Instant(); 
                break;
            case BuildMethod.typwriter:
                Prepare_Typewriter();
                break;
            case BuildMethod.fade:
                Prepare_Fade();
                break;
        }
    }

    private void Prepare_Instant()
    {
        tmpro.color = tmpro.color;
        tmpro.text = fullTargetText;
        tmpro.ForceMeshUpdate();
        tmpro.maxVisibleCharacters = tmpro.textInfo.characterCount;
    }

    private void Prepare_Typewriter()
    {
        tmpro.color = tmpro.color;
        tmpro.maxVisibleCharacters = 0;
        tmpro.text = preText;

        if (preText != "")
        {
            tmpro.ForceMeshUpdate();
            tmpro.maxVisibleCharacters = tmpro.textInfo.characterCount;
        }

        tmpro.text += targetText;
        tmpro.ForceMeshUpdate();
    }

    private void Prepare_Fade()
    {

    }

    private IEnumerator Build_Typewriter()
    {
        while(tmpro.maxVisibleCharacters < tmpro.textInfo.characterCount) 
        {
            tmpro.maxVisibleCharacters += hurryUp ? characterPerCycle * 5 : characterPerCycle;

            yield return new WaitForSeconds(0.015f / speed);
        }
    }

    private IEnumerator Build_Fade()
    {
        yield return null;
    }
}
