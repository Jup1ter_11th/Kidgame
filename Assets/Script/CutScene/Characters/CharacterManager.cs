using DIALOGUE;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager instance { get; private set; }

    public Character[] allCharacters => characters.Values.ToArray();
    private Dictionary<string, Character> characters = new Dictionary<string, Character>();

    private CharacterConfigSO config => DialogueSystem.instance.config.characterConfigurationAsset;

    private const string CHARACTER_CASTING_ID = " as ";

    private const string CHARACTERNAME_ID = "<charname>";
    public string characterRootPathFormat => $"Characters/{CHARACTERNAME_ID}";
    public string characterPrefabNameFormat => $"Character - [{CHARACTERNAME_ID}]";
    public string characterPrefabPathFormat => $"{characterRootPathFormat}/{characterPrefabNameFormat}";

    [SerializeField]private RectTransform _characterpanel = null;
    public RectTransform characterPanel => _characterpanel;

    private void Awake()
    {
        instance = this;
    }

    public CharacterConfigData GetCharacterConfig(string characterName)
    {
        return config.GetConfig(characterName);
    }

    public Character GetCharacter(string characterName, bool createIfDoesNotExist = false)
    {
        if (characters.ContainsKey(characterName.ToLower()))
            return characters[characterName.ToLower()];
        else if (createIfDoesNotExist)
            return CreateCharacter(characterName);

        return null;
    }

    public bool HasCharacter(string characterName) => characters.ContainsKey(characterName.ToLower());

    public Character CreateCharacter(string charaterName, bool revealAfterCreation = false)
    {
        if (characters.ContainsKey(charaterName.ToLower()))
        {
            Debug.LogWarning($"A Character called '{charaterName}' already exits. Did not create this character.");
            return null;
        }

        CHARACTER_INFO info = GetCharacterInfo(charaterName);

        Character character = CreateCharacterFromInfo(info);

        characters.Add(info.name.ToLower(), character);

        if (revealAfterCreation)
            character.Show();

        return character;
    }

    private CHARACTER_INFO GetCharacterInfo(string characterName)
    {
        CHARACTER_INFO result = new CHARACTER_INFO();

        string[] nameData = characterName.Split(CHARACTER_CASTING_ID, System.StringSplitOptions.RemoveEmptyEntries);

        result.name = nameData[0];
        result.castingName = nameData.Length > 1 ? nameData[1] : result.name;

        result.config = config.GetConfig(result.castingName);

        result.prefab = GetPrefabForCharacter(result.castingName);

        result.rootCharacterFolder = FormatCharacterPath(characterRootPathFormat, result.castingName);

        return result;
    }

    private GameObject GetPrefabForCharacter(string characterName)
    {
        string prefabPath = FormatCharacterPath(characterPrefabPathFormat, characterName);
        return Resources.Load<GameObject>(prefabPath);
    }

    public string FormatCharacterPath(string path, string characterName) => path.Replace(CHARACTERNAME_ID, characterName);

    private Character CreateCharacterFromInfo(CHARACTER_INFO info)
    {
        CharacterConfigData config = info.config;

        switch(config.characterType)
        {
            case Character.CharacterType.Text:
                return new Character_Text(info.name, config);

            case Character.CharacterType.Sprite:
            case Character.CharacterType.SpriteSheet:
                return new Character_Sprite(info.name, config, info.prefab, info.rootCharacterFolder);

            case Character.CharacterType.Live2D:
                return new Character_Live2D(info.name, config, info.prefab, info.rootCharacterFolder);

            case Character.CharacterType.Model3D:
                return new Character_Model3D(info.name, config, info.prefab, info.rootCharacterFolder);

            default: return null;
        }
    }

    public void SortCharacters()
    {
        List<Character> activeCharacters = characters.Values.Where(c => c.root.gameObject.activeInHierarchy && c.IsVisible).ToList();
        List<Character> inactiveCharacters = characters.Values.Except(activeCharacters).ToList();

        activeCharacters.Sort((a, b) => a.priority.CompareTo(b.priority));
        activeCharacters.Concat(inactiveCharacters);

        SortCharacter(activeCharacters);
    }

    public void SortCharacters(string[] characterNames)
    {
        List<Character> sortedCharacter = new List<Character>();

        sortedCharacter = characterNames
            .Select(name => GetCharacter(name))
            .Where(character => character != null)
            .ToList();

        List<Character> remainingCharacters = characters.Values
            .Except(sortedCharacter)
            .OrderBy(character => character.priority)
            .ToList();

        sortedCharacter.Reverse();

        int startingPriority = remainingCharacters.Count > 0 ? remainingCharacters.Max(c => c.priority) : 0;
        for(int i = 0; i < sortedCharacter.Count; i++)
        {
            Character character = sortedCharacter[i];
            character.SetPrioity(startingPriority + i + 1, autoSortCharacterOnUI: false);
        }
        
        List<Character> allCharacters = remainingCharacters.Concat(sortedCharacter).ToList();
        SortCharacter(allCharacters);
    }

    private void SortCharacter(List<Character> characterSortingOrder)
    {
        int i = 0;
        foreach (Character character in characterSortingOrder)
        {
            character.root.SetSiblingIndex(i++);
        }
    }

    private class CHARACTER_INFO
    {
        public string name = "";
        public string castingName = "";

        public string rootCharacterFolder = "";

        public CharacterConfigData config = null;

        public GameObject prefab = null;
    }
}
