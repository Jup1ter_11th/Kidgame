using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class DL_Dialogue_Data
{
    public List<Dialoge_Segment> segments;
    private const string segmentIdentifierPattern = @"\{[ca]\}|\{w[ca]\s\d*\.?\d*\}";

    public DL_Dialogue_Data(string rawDialogue) 
    {
        segments = RipSegment(rawDialogue);
    }

    public List<Dialoge_Segment> RipSegment(string rawDialogue)
    {
        List<Dialoge_Segment> segments = new List<Dialoge_Segment>();
        MatchCollection matches = Regex.Matches(rawDialogue, segmentIdentifierPattern);

        int lastIndex = 0;
        //Find the first or only segment int the file
        Dialoge_Segment segment = new Dialoge_Segment();
        segment.dialogue = (matches.Count == 0 ? rawDialogue : rawDialogue.Substring(0, matches[0].Index));
        segment.startSignal = Dialoge_Segment.StartSignal.NONE;
        segment.signalDelay = 0;
        segments.Add(segment);

        if (matches.Count == 0)
            return segments;
        else
            lastIndex = matches[0].Index;

        for (int i = 0; i < matches.Count; i++) 
        { 
            Match match = matches[i];
            segment = new Dialoge_Segment();

            //Get the start sinal for the segment
            string signalMatch = match.Value;//{A}
            signalMatch = signalMatch.Substring(1, match.Length - 2);
            string[] signalSplit = signalMatch.Split(' ');

            segment.startSignal = (Dialoge_Segment.StartSignal) Enum.Parse(typeof(Dialoge_Segment.StartSignal), signalSplit[0].ToUpper());

            //Get the signal delay
            if (signalSplit.Length > 1)
                float.TryParse(signalSplit[1], out segment.signalDelay);

            //Get the dialogue for the segment
            int nextIndex = i + 1 < matches.Count ? matches[i + 1].Index : rawDialogue.Length;
            segment.dialogue = rawDialogue.Substring(lastIndex + match.Length, nextIndex - (lastIndex + match.Length));
            lastIndex = nextIndex;

            segments.Add(segment);
        }

        return segments;
    }

    public struct Dialoge_Segment
    {
        public string dialogue;
        public StartSignal startSignal;
        public float signalDelay;

        public enum StartSignal { NONE, C, A, WC, WA}

        public bool appendText => (startSignal == StartSignal.A || startSignal == StartSignal.WA);
    }
}
