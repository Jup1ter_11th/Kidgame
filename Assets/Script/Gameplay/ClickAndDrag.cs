using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ClickAndDrag : MonoBehaviour
{
    public GameObject selectedObject;
    Vector3 offset;

    public List<MatchingObject> allObject;

    public Button nextButton;

    public AudioClip voice;

    public float timelimit = 120;
    public GameObject timeBar;
    public Slider timeSlider;

    void Start()
    {
        nextButton.gameObject.SetActive(false);

        AudioManager.instance.PlayTrack(FilePath.resources_music + "BGM", volumeCap: 0.1f);

        StartCoroutine(StartScene(voice));
    }

    void Update()
    {
        GameManager.UpdateTime();

        timeSlider.value = 1 - (GameManager.TimeInSecond / timelimit);

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            Collider2D targetObject = Physics2D.OverlapPoint(mousePosition);

            if (targetObject)
            {
                for (int i = 0; i < allObject.Count; i++)
                {
                    if (allObject[i].GetComponent<Collider2D>() == targetObject)
                    {
                        if (allObject[i].isCorrect == false || allObject[i].transform.position != allObject[i].targetObject.transform.position && allObject[i].isCorrect == true)
                        {
                            selectedObject = targetObject.transform.gameObject;
                            selectedObject.transform.parent = null;
                            offset = selectedObject.transform.position - mousePosition;
                        }
                    }
                }
            }

        }
        if (selectedObject)
        {
            selectedObject.transform.position = mousePosition + offset;
        }
        if (Input.GetMouseButtonUp(0) && selectedObject)
        {  

            for (int i = 0; i < allObject.Count; i++)
            {
                if (allObject[i].isCorrect)
                {
                    allObject[i].transform.position = allObject[i].targetObject.transform.position;
                    allObject.RemoveAt(i);
                    GameManager.AddScore(1);
                    AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Correct_answer", voloume: 0.5f);
                    GameManager.instance.CorrectSFX();
                }
            }

            if (selectedObject.GetComponent<MatchingObject>().isCorrect == false)
            {
                GameManager.instance.IncorrectSFX();
            }

            selectedObject = null;
        }

        if (allObject.Count == 0)
        {
            nextButton.gameObject.SetActive(true);
            timeBar.gameObject.SetActive(false);
        }

        if (GameManager.TimeInSecond >= timelimit)
        {
            nextButton.gameObject.SetActive(true);
            timeBar.gameObject.SetActive(false);

            for (int i = 0; i < allObject.Count; i++)
            {
                allObject[i].gameObject.SetActive(false);
                allObject.RemoveAt(i);
            }
        }
    }

    public void NextScene(string sceneName)
    {
        AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Click", voloume: 0.5f);
        StartCoroutine(GraphicPanelManager.instance.TransitionScene(sceneName));
    }

    public IEnumerator StartScene(AudioClip vc = null)
    {
        GraphicPanel black = GraphicPanelManager.instance.GetPanel("Transition");
        GraphicLayer blackLayer = black.GetLayer(0, true);

        yield return blackLayer.SetTexture(FilePath.resources_backgroundImages + "Black", immediate: true);

        yield return blackLayer.Clear(transitionspeed: 1f);

        if (vc != null) 
        {
            AudioManager.instance.PlayVoice(vc);
        }     
    }
}
