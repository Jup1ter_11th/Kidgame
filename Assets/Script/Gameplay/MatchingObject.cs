using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchingObject : MonoBehaviour
{
    [SerializeField]
    public GameObject targetObject = null;

    public bool isCorrect;

    void Start()
    {
        isCorrect = false;
    }

    // Update is called once per frame
    void Update()
    {
        float Distance = Vector3.Distance(transform.position, targetObject.transform.position);

        if (Distance < 1)
        {
            isCorrect = true;
        }
        else
        {
            isCorrect = false;
        }

    }
}
