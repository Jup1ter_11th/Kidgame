using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoresummary : MonoBehaviour
{
    public GameObject star2;
    public GameObject star3;


    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.StopTrack(0);
        StartCoroutine(GraphicPanelManager.instance.StartScene());
        AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Cheer", voloume: 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.GetInt("score") > 4 && PlayerPrefs.GetInt("score") <= 10)
        {
            star2.gameObject.SetActive(true);
            star3.gameObject.SetActive(false);
        }
        else if (PlayerPrefs.GetInt("score") > 10)
        {
            star2.gameObject.SetActive(true);
            star3.gameObject.SetActive(true);
        }
    }

    public void NextScene(string sceneName)
    {
        AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Click", voloume: 0.5f);
        StartCoroutine(GraphicPanelManager.instance.TransitionScene(sceneName));
    }

    public void ExitApp()
    {
        Application.Quit();
    }
}
