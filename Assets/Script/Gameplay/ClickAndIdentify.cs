using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ClickAndIdentify : MonoBehaviour
{
    public GameObject selectedObject;

    public List<GameObject> AllObject;

    public Button nextButton;

    public float timelimit = 120;
    public GameObject timeBar;
    public Slider timeSlider;

    void Start()
    {
        foreach (var obj in AllObject)
        {
            obj.gameObject.GetComponent<Renderer>().enabled = false;
        }
        nextButton.gameObject.SetActive(false);

        AudioManager.instance.PlayTrack(FilePath.resources_music + "BGM", volumeCap: 0.1f);

        StartCoroutine(StartScene());
    }

    void Update()
    {
        GameManager.UpdateTime();

        timeSlider.value = 1 - (GameManager.TimeInSecond / timelimit);

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0) && AllObject.Count != 0)
        {
            Collider2D targetObject = Physics2D.OverlapPoint(mousePosition);
            if (targetObject)
            {
                selectedObject = targetObject.transform.gameObject;
                
            }
            else
            {
                GameManager.instance.IncorrectSFX();
            }
        }
        if (selectedObject)
        {
            selectedObject.GetComponent<Renderer>().enabled = true;
            selectedObject.GetComponent<PairedObject>().pairWith.GetComponent<Renderer>().enabled = true;

            for (int i = 0; i < AllObject.Count; i++)
            {
                if (selectedObject == AllObject[i])
                {
                    AllObject.RemoveAt(i);

                    GameManager.AddScore(1);
                    AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Correct_answer1", voloume: 0.5f);
                    GameManager.instance.CorrectSFX();
                }

                if (selectedObject.GetComponent<PairedObject>().pairWith == AllObject[i])
                {
                    AllObject.RemoveAt(i);
                }
            }
        }
        if (Input.GetMouseButtonUp(0) && selectedObject)
        {
            selectedObject = null;
        }

        if (AllObject.Count == 0)
        {
            nextButton.gameObject.SetActive(true);
            timeBar.gameObject.SetActive(false);
        }
        if (GameManager.TimeInSecond >= timelimit)
        {
            nextButton.gameObject.SetActive(true);
            timeBar.gameObject.SetActive(false);

            for (int i = 0; i < AllObject.Count; i++)
            {
                AllObject[i].gameObject.SetActive(false);
                AllObject.RemoveAt(i);
            }
        }
    }

    public void NextScene(string sceneName)
    {
        AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Click", voloume: 0.5f);
        StartCoroutine(GraphicPanelManager.instance.TransitionScene(sceneName));
    }

    public IEnumerator StartScene()
    {
        GraphicPanel black = GraphicPanelManager.instance.GetPanel("Transition");
        GraphicLayer blackLayer = black.GetLayer(0, true);

        yield return blackLayer.SetTexture(FilePath.resources_backgroundImages + "Black", immediate: true);

        yield return blackLayer.Clear(transitionspeed: 1f);

        AudioManager.instance.PlayVoice(FilePath.resources_voices + "���§�ҡ��˹�� 11");
    }
}
