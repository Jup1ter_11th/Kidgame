using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelect : MonoBehaviour
{
    public GameObject selectedObject;
    public GameObject boy;
    public GameObject girl;

    public Button nextButton;

    void Start()
    {
        nextButton.gameObject.SetActive(false);

        StartCoroutine(StartScene());

        
    }

    void Update()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            Collider2D targetObject = Physics2D.OverlapPoint(mousePosition);

            if (targetObject)
            {
                selectedObject = targetObject.transform.gameObject;
                selectedObject.GetComponent<SpriteRenderer>().enabled = true;
                nextButton.gameObject.SetActive(true);

                if (selectedObject == boy)
                {
                    girl.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerPrefs.SetString("gender", "boy");
                }
                    

                if (selectedObject == girl)
                {
                    boy.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerPrefs.SetString("gender", "girl");
                }
                    
            }
        }


    }

    public void NextScene(string sceneName)
    {
        AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Click", voloume: 0.5f);
        AudioManager.instance.StopSoundEffect("˹��3 Scene���͵���Ф�");
        StartCoroutine(GraphicPanelManager.instance.TransitionScene(sceneName));
    }

    public IEnumerator StartScene()
    {
        GraphicPanel black = GraphicPanelManager.instance.GetPanel("Transition");
        GraphicLayer blackLayer = black.GetLayer(0, true);

        yield return blackLayer.SetTexture(FilePath.resources_backgroundImages + "Black", immediate: true);

        yield return blackLayer.Clear(transitionspeed: 1f);

        AudioManager.instance.PlayVoice(FilePath.resources_voices + "˹��3 Scene���͵���Ф�");
    }
}
