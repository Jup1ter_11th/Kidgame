using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    private const string SFX_PARENT_NAME = "SFX";
    private const string SFX_NAME_FORMAT = "SFX - [{0}]";
    public const float TRACK_TRASITION_SPEED = 1f;
    public static AudioManager instance { get; private set; }

    private Dictionary<int, AudioChannel> channels = new Dictionary<int, AudioChannel>();

    public AudioMixerGroup musicMixer;
    public AudioMixerGroup sfxMixer;
    public AudioMixerGroup voiceMixer;

    private Transform sfxRoot;

    private void Awake()
    {
        if (instance == null)
        {
            transform.SetParent(null);
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
            return;
        }

        sfxRoot = new GameObject(SFX_PARENT_NAME).transform;
        sfxRoot.SetParent(transform);
    }

    public AudioSource PlaySoundEffect(string filepath, AudioMixerGroup mixer = null, float voloume = 1f, float pitch = 1f, bool loop = false)
    {
        AudioClip clip = Resources.Load<AudioClip>(filepath);

        if (clip == null)
        {
            Debug.LogError($"Could not load audio file '{filepath}'. Please make sure this exits in Resource directory");
            return null;
        }

        return PlaySoundEffect(clip, mixer, voloume, pitch, loop);
    }

    public AudioSource PlaySoundEffect(AudioClip clip, AudioMixerGroup mixer = null, float voloume = 1f, float pitch = 1f, bool loop = false)
    {
        AudioSource effectSource = new GameObject(string.Format(SFX_NAME_FORMAT, clip.name)).AddComponent<AudioSource>();
        effectSource.transform.SetParent(sfxRoot);
        effectSource.transform.position = sfxRoot.position;

        effectSource.clip = clip;

        if (mixer == null)
        {
            mixer = sfxMixer;
        }

        effectSource.outputAudioMixerGroup = mixer;
        effectSource.volume = voloume;
        effectSource.spatialBlend = 0;
        effectSource.pitch = pitch;
        effectSource.loop = loop;
        
        effectSource.Play();

        if (!loop)
        {
            Destroy(effectSource.gameObject, (clip.length / pitch) + 1);
        }

        return effectSource;
    }

    public AudioSource PlayVoice(string filepath, float voloume = 1f, float pitch = 1f, bool loop = false)
    {
        return PlaySoundEffect(filepath, voiceMixer, voloume, pitch, loop);
    }
    
    public AudioSource PlayVoice(AudioClip clip, float voloume = 1f, float pitch = 1f, bool loop = false)
    {
        return PlaySoundEffect(clip, voiceMixer, voloume, pitch, loop);
    }

    public void StopSoundEffect(AudioClip clip) => StopSoundEffect(clip.name);

    public void StopSoundEffect(string soundName)
    {
        soundName = soundName.ToLower();

        AudioSource[] sources = new[] { sfxRoot.GetComponentInChildren<AudioSource>() };
        foreach (var source in sources)
        {
            if (source.clip.name.ToLower() == soundName)
            {
                Destroy(source.gameObject);
                return;
            }
        }
    }

    public AudioTrack PlayTrack(string filePath, int channel = 0, bool loop = true, float stratingVolume = 0f, float volumeCap = 1f, float pitch = 1f)
    {
        AudioClip clip = Resources.Load<AudioClip>(filePath);

        if (clip == null)
        {
            Debug.LogError($"Could not load audio file '{filePath}'. Please make sure this exits in Resource directory");
            return null;
        }

        return PlayTrack(clip, channel, loop, stratingVolume, volumeCap, pitch, filePath);
    }
    
    public AudioTrack PlayTrack(AudioClip clip, int channel = 0, bool loop = true, float stratingVolume = 0f, float volumeCap = 1f, float pitch = 1f,string filePath = "")
    {
        AudioChannel audioChannel = TryGetChannel(channel, createIfNotExist: true);
        AudioTrack track = audioChannel.PlayTrack(clip, loop, stratingVolume, volumeCap, pitch,filePath);
        return track;
    }

    public void StopTrack(int channel)
    {
        AudioChannel c = TryGetChannel(channel, createIfNotExist: false);

        if (c == null)
            return;

        c.StopTrack();
    }

    public void StopTrack(string trackName)
    {
        trackName = trackName.ToLower();

        foreach (var channel in channels.Values)
        {
            channel.StopTrack();
            return;
        }
    }

    public AudioChannel TryGetChannel(int channelNumber, bool createIfNotExist = false)
    {
        AudioChannel channel = null;

        if (channels.TryGetValue(channelNumber, out channel))
        {
            return channel;
        }
        else if (createIfNotExist)
        {
            channel = new AudioChannel(channelNumber);
            channels.Add(channelNumber, channel);
            return channel;
        }

        return null;
    }
}
