using UnityEngine;
using UnityEngine.Audio;
[CreateAssetMenu(menuName = "GameDev3/Chapter11/SoundSettingsPreset",
    fileName = "SoundSettingsPreset")]
public class SoundSettings : ScriptableObject
{
    public AudioMixer AudioMixer;
    [Header("MasterVolume")]
    public string MasterVolumeName = "MasterVolume";
    [Range(-80,20)]
    public float MasterVolume;
    [Header("MusicVolume")]
    public string MusicVolumeName = "MusicVolume";
    [Range(-80,20)]
    public float MusicVolume;
    [Header("SFXVolume")]
    public string SFXVolumeName = "SFXVolume";
    [Range(-80,20)]
    public float SFXVolume;
    [Header("VoicesVolume")]
    public string VoicesVolumeName = "VoicesVolume";
    [Range(-80,20)]
    public float VoicesVolume;
}