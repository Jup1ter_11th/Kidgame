using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CMD_DatabaseExtension_Characters : CMD_DatabaseExtension
{
    private static string[] PARAM_ENABLE => new string[] { "-e", "-enabled" };
    private static string[] PARAM_IMMEDIATE => new string[] { "-i", "-immediate" };
    private static string[] PARAM_SPEED => new string[] { "-spd", "-speed" };
    private static string[] PARAM_SMOOTH => new string[] { "-sm", "-smooth" };
    private static string PARAM_XPOS => "-x";
    private static string PARAM_YPOS => "-y";

    new public static void Extend(CommandDatabase database)
    {
        database.AddCommand("create", new Action<string[]>(CreateCharacter));
        database.AddCommand("move", new Func<string[], IEnumerator>(MoveCharacter));
        database.AddCommand("show", new Func<string[], IEnumerator>(ShowAll));
        database.AddCommand("hide", new Func<string[], IEnumerator>(HideAll));
        database.AddCommand("highlight", new Func<string[], IEnumerator>(HighlighAll));
        database.AddCommand("unhighlight", new Func<string[], IEnumerator>(UnHighlighAll));
        database.AddCommand("sort", new Action<string[]>(Sort));
        database.AddCommand("animate", new Func<string[], IEnumerator>(SetAnimate));

        //Add commands to characters
        CommandDatabase baseCommands = CommandManager.instance.CreateSubDatabase(CommandManager.DATABASE_CHARACTER_BASE);
        baseCommands.AddCommand("move", new Func<string[], IEnumerator>(MoveCharacter));
        baseCommands.AddCommand("show", new Func<string[], IEnumerator>(Show));
        baseCommands.AddCommand("hide", new Func<string[], IEnumerator>(Hide));
        baseCommands.AddCommand("highlight", new Func<string[], IEnumerator>(Highlight));
        baseCommands.AddCommand("Unhighlight", new Func<string[], IEnumerator>(UnHighlight));
        baseCommands.AddCommand("setpriority", new Action<string[]>(SetPriority));
        baseCommands.AddCommand("setcolor", new Func<string[], IEnumerator>(SetColor));
    }

    private static void CreateCharacter(string[] data)
    {
        string characterName = data[0];
        bool enable = false;
        bool immediate = false;

        var parameters = ConvertDataToParameters(data);

        parameters.TryGetValue(PARAM_ENABLE, out enable, defaultValue: false);
        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        Character character = CharacterManager.instance.CreateCharacter(characterName);

        if (!enable)
            return;

        if (immediate)
            character.IsVisible = true;
        else
            character.Show();
    }

    private static IEnumerator MoveCharacter(string[] data)
    {
        string characterName = data[0];
        Character character = CharacterManager.instance.GetCharacter(characterName);

        if (character == null)
            yield break;

        float x = 0, y = 0;
        float speed = 1;
        bool smooth = false;
        bool immediate = false;

        var parameters = ConvertDataToParameters(data);

        //try to get the x axis position
        parameters.TryGetValue(PARAM_XPOS, out x);

        //try to get the y axis position
        parameters.TryGetValue(PARAM_YPOS, out y);

        //try to get the speed
        parameters.TryGetValue(PARAM_SPEED, out speed, defaultValue: 1);

        //try to get the smoothing
        parameters.TryGetValue(PARAM_SMOOTH, out smooth, defaultValue: false);

        //try to get the immediate setting of position
        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        Vector2 position = new Vector2(x, y);

        if (immediate)
            character.SetPosition(position);
        else
        {
            CommandManager.instance.AddterminationActionToCurrentProcess(() => { character?.SetPosition(position); });
            yield return character.MoveToPosition(position, speed, smooth);
        }
            
    }

    private static IEnumerator ShowAll(string[] data)
    {
        List<Character> characters = new List<Character>();
        bool immediate = false;

        foreach (string s in data)
        {
            Character character = CharacterManager.instance.GetCharacter(s, createIfDoesNotExist : false);
            if (character != null)
                characters.Add(character);
        }

        if (characters.Count == 0)
            yield break;

        //Convert the data array to a parameter container
        var parameters = ConvertDataToParameters(data);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        //Call the logic on all the characters
        foreach (Character character in characters)
        {
            if (immediate)
                character.IsVisible = true;
            else
                character.Show();
        }

        if (!immediate)
        {
            CommandManager.instance.AddterminationActionToCurrentProcess(() =>
            {
                foreach (Character character in characters)
                    character.IsVisible = true;
            });

            while(characters.Any(c => c.isRevealing))
                yield return null;
        }
    }

    private static IEnumerator HideAll(string[] data)
    {
        List<Character> characters = new List<Character>();
        bool immediate = false;

        foreach (string s in data)
        {
            Character character = CharacterManager.instance.GetCharacter(s, createIfDoesNotExist: false);
            if (character != null)
                characters.Add(character);
        }

        if (characters.Count == 0)
            yield break;

        //Convert the data array to a parameter container
        var parameters = ConvertDataToParameters(data);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        //Call the logic on all the characters
        foreach (Character character in characters)
        {
            if (immediate)
                character.IsVisible = false;
            else
                character.Hide();
        }

        if (!immediate)
        {
            CommandManager.instance.AddterminationActionToCurrentProcess(() =>
            {
                foreach (Character character in characters)
                    character.IsVisible = false;
            });

            while (characters.Any(c => c.isHiding))
                yield return null;
        }

    }

    private static IEnumerator Show(string[] data)
    {
        Character character = CharacterManager.instance.GetCharacter(data[0]);

        if (character == null)
            yield break;

        bool immediate = false;
        var parameters = ConvertDataToParameters(data);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        if (immediate)
            character.IsVisible = true;
        else
        {
            //A long running process should have a stop action to cancel out the coroutine and run logic that should complete this commadn
            CommandManager.instance.AddterminationActionToCurrentProcess(() => { if (character != null) character.IsVisible = true; });

            yield return character.Show();
        }
    }

    private static IEnumerator Hide(string[] data)
    {
        Character character = CharacterManager.instance.GetCharacter(data[0]);

        if (character == null)
            yield break;

        bool immediate = false;
        var parameters = ConvertDataToParameters(data);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        if (immediate)
            character.IsVisible = false;
        else
        {
            //A long running process should have a stop action to cancel out the coroutine and run logic that should complete this commadn
            CommandManager.instance.AddterminationActionToCurrentProcess(() => { if (character != null) character.IsVisible = false; });

            yield return character.Hide();
        }
    }

    private static IEnumerator Highlight(string[] data)
    {
        //format: setSprite(character sprite)
        Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false) as Character;

        if (character == null)
            yield break;

        bool immediate = false;
        var parameters = ConvertDataToParameters(data, startingIdex: 1);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        if(immediate)
        {
            character.Highlight(immediate: true);
        }
        else
        {
            //A long running process should have a stop action to cancel out the coroutine and run logic that should complete this commadn
            CommandManager.instance.AddterminationActionToCurrentProcess(() => { character?.Highlight(immediate: true); });

            yield return character.Highlight();
        }
    }

    private static IEnumerator UnHighlight(string[] data)
    {
        //format: setSprite(character sprite)
        Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false) as Character;

        if (character == null)
            yield break;

        bool immediate = false;
        var parameters = ConvertDataToParameters(data, startingIdex: 1);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        if (immediate)
        {
            character.UnHighlight(immediate: true);
        }
        else
        {
            //A long running process should have a stop action to cancel out the coroutine and run logic that should complete this commadn
            CommandManager.instance.AddterminationActionToCurrentProcess(() => { character?.UnHighlight(immediate: true); });

            yield return character.UnHighlight();
        }
    }

    private static IEnumerator HighlighAll(string[] data)
    {
        List<Character> characters = new List<Character>();
        bool immediate = false;
        bool handleUnspecifiedCharacter = true;
        List<Character> unspecifiedCharacters = new List<Character>();

        //Add any characters specified to be highlighted
        for (int i = 0; i < data.Length; i++)
        {
            Character character = CharacterManager.instance.GetCharacter(data[i], createIfDoesNotExist: false);
            if (character != null)
                characters.Add(character);
        }

        if (characters.Count == 0)
            yield break;

        //Grab the extra parameters
        var parameters = ConvertDataToParameters(data, startingIdex: 1);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);
        parameters.TryGetValue(new string[] { "-o", "-only" }, out handleUnspecifiedCharacter, defaultValue: true);

        //Make all characters perform the logic
        foreach (Character character in characters)
        {
            character.Highlight(immediate: immediate);
        }

        //if we are forcing any unspecified characters to use the opposite hightlighted status
        if (handleUnspecifiedCharacter)
        {
            foreach (Character character in CharacterManager.instance.allCharacters)
            {
                if (characters.Contains(character))
                    continue;

                unspecifiedCharacters.Add(character);
                character.UnHighlight(immediate: immediate);
            }
        }

        //Wait for all characters to finish highlighting
        if (!immediate)
        {
            CommandManager.instance.AddterminationActionToCurrentProcess(() =>
            {
                foreach (var character in characters)
                    character.Highlight(immediate: true);

                if (!handleUnspecifiedCharacter) return;

                foreach (var character in unspecifiedCharacters)
                    character.UnHighlight(immediate: true);

            });

            while (characters.Any(c => c.isHightlighting) || (handleUnspecifiedCharacter && unspecifiedCharacters.Any(uc => uc.isUnHightlighting)))
                yield return null;
        }
    }

    private static IEnumerator UnHighlighAll(string[] data)
    {
        List<Character> characters = new List<Character>();
        bool immediate = false;
        bool handleUnspecifiedCharacter = true;
        List<Character> unspecifiedCharacters = new List<Character>();

        //Add any characters specified to be highlighted
        for (int i = 0; i < data.Length; i++)
        {
            Character character = CharacterManager.instance.GetCharacter(data[i], createIfDoesNotExist: false);
            if (character != null)
                characters.Add(character);
        }

        if (characters.Count == 0)
            yield break;

        //Grab the extra parameters
        var parameters = ConvertDataToParameters(data, startingIdex: 1);

        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);
        parameters.TryGetValue(new string[] { "-o", "-only" }, out handleUnspecifiedCharacter, defaultValue: true);

        //Make all characters perform the logic
        foreach (Character character in characters)
        {
            character.UnHighlight(immediate: immediate);
        }

        //if we are forcing any unspecified characters to use the opposite hightlighted status
        if (handleUnspecifiedCharacter)
        {
            foreach (Character character in CharacterManager.instance.allCharacters)
            {
                if (characters.Contains(character))
                    continue;

                unspecifiedCharacters.Add(character);
                character.Highlight(immediate: immediate);
            }
        }

        //Wait for all characters to finish highlighting
        if (!immediate)
        {
            CommandManager.instance.AddterminationActionToCurrentProcess(() =>
            {
                foreach (var character in characters)
                    character.UnHighlight(immediate: true);

                if (!handleUnspecifiedCharacter) return;

                foreach (var character in unspecifiedCharacters)
                    character.Highlight(immediate: true);

            });

            while (characters.Any(c => c.isUnHightlighting) || (handleUnspecifiedCharacter && unspecifiedCharacters.Any(uc => uc.isHightlighting)))
                yield return null;
        }
    }

    private static void SetPriority(string[] data)
    {
        Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
        int priority;

        if (character == null || data.Length < 2)
            return;

        if (!int.TryParse(data[1], out priority))
            priority = 0;

        character.SetPrioity(priority);
    }

    private static void Sort(string[] data)
    {
        CharacterManager.instance.SortCharacters(data);
    }

    public static IEnumerator SetColor(string[] data)
    {
        Character character = CharacterManager.instance.GetCharacter(data[0], createIfDoesNotExist: false);
        string colorName;
        float speed;
        bool immediate;

        if (character == null || data.Length < 2)
            yield break;

        //Grab the parameters
        var parameters = ConvertDataToParameters(data, startingIdex: 1);

        //Try to get the color name
        parameters.TryGetValue(new string[] { "-c", "-color" }, out colorName);
        //Try to get the speed of the transition
        bool specificSpeed = parameters.TryGetValue(PARAM_SPEED, out speed, defaultValue: 1f);
        //Try to get the instant value
        if (!specificSpeed)
            parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: true);
        else
            immediate = false;

        //Get the color value from the name
        Color color = Color.white;
        color = color.GetColorFromName(colorName);

        if (immediate)
            character.SetColor(color);
        else
        {
            CommandManager.instance.AddterminationActionToCurrentProcess(() => { character?.SetColor(color); });
            character.TransitionColor(color, speed);
        }

        yield break;
    }

    private static IEnumerator SetAnimate(string[] data)
    {
        string characterName = data[0];
        Character character = CharacterManager.instance.GetCharacter(characterName);
        
        string animation;

        //Grab the parameters
        var parameters = ConvertDataToParameters(data);

        //Try to get the animation name
        parameters.TryGetValue(new string[] { "-a", "-animation" }, out animation);
        
        character.Animate(animation);

        yield return null;
    }
}
