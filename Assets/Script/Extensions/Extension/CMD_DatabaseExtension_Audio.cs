using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMD_DatabaseExtension_Audio : CMD_DatabaseExtension
{
    private static string[] PARAM_SFX = new string[] { "-s", "-sfx" };
    private static string[] PARAM_VOLUME = new string[] { "-v", "-vol", "-volume" };
    private static string[] PARAM_PITCH = new string[] { "-p", "-pitch" };
    private static string[] PARAM_LOOP = new string[] { "-l", "-loop" };

    private static string[] PARAM_CHANNEL = new string[] { "-c", "-channel" };
    private static string[] PARAM_IMMEDIATE = new string[] { "-i", "-immediate" };
    private static string[] PARAM_START_VOLUME = new string[] { "-sv", "-startvolume" };
    private static string[] PARAM_SONG = new string[] { "-s", "-song" };
    private static string[] PARAM_AMBIENCE = new string[] { "-a", "-ambience" };

    new public static void Extend(CommandDatabase database)
    {
        database.AddCommand("playsfx", new Func<string[], IEnumerator>(PlaySFX));
        database.AddCommand("stopsfx", new Action<string>(StopSFX));

        database.AddCommand("playvoice", new Func<string[], IEnumerator>(PlayVoice));
        database.AddCommand("stopvoice", new Action<string>(StopVoice));

        database.AddCommand("playmusic", new Func<string[], IEnumerator>(PlayMusic));
        database.AddCommand("stopmusic", new Action<string>(StopMusic));

        database.AddCommand("playambience", new Func<string[], IEnumerator>(PlayAmbience));
        database.AddCommand("stopambience", new Action<string>(StopAmbience));
    }

    private static IEnumerator PlaySFX(string[] data)
    {
        string filepath;
        float volume, pitch;
        bool loop;

        var parameters = ConvertDataToParameters(data);

        //try to get the name or path to the sound effect
        parameters.TryGetValue(PARAM_SFX, out filepath);

        //try to get the volume of the sound
        parameters.TryGetValue(PARAM_VOLUME, out volume, defaultValue: 1f);

        //try to get the pitch of the sound
        parameters.TryGetValue(PARAM_PITCH, out pitch, defaultValue: 1f);

        //try to get if this sound loops
        parameters.TryGetValue(PARAM_LOOP, out loop, defaultValue: false);

        //Run the logic
        AudioClip sound = Resources.Load<AudioClip>(FilePath.GetPathToResource(FilePath.resources_sfx, filepath));

        if (sound == null)
        {
            Debug.Log($"Was not able to load SFX '{filepath}'");
            yield break;
        }
            

        yield return AudioManager.instance.PlaySoundEffect(sound, voloume: volume, pitch: pitch, loop: loop);
    }

    private static void StopSFX(string data)
    {
        AudioManager.instance.StopSoundEffect(data);
    }

    private static IEnumerator PlayVoice(string[] data)
    {
        string filepath;
        float volume, pitch;
        bool loop;

        var parameters = ConvertDataToParameters(data);

        //try to get the name or path to the voice
        parameters.TryGetValue(PARAM_SFX, out filepath);

        //try to get the volume of the sound
        parameters.TryGetValue(PARAM_VOLUME, out volume, defaultValue: 1f);

        //try to get the pitch of the sound
        parameters.TryGetValue(PARAM_PITCH, out pitch, defaultValue: 1f);

        //try to get if this sound loops
        parameters.TryGetValue(PARAM_LOOP, out loop, defaultValue: false);

        //Run the logic
        AudioClip sound = Resources.Load<AudioClip>(FilePath.GetPathToResource(FilePath.resources_voices, filepath));

        if (sound == null)
        {
            Debug.Log($"Was not able to load Voice '{filepath}'");
            yield break;
        }

        yield return AudioManager.instance.PlaySoundEffect(sound, voloume: volume, pitch: pitch, loop: loop);
    }

    private static void StopVoice(string data)
    {
        AudioManager.instance.StopSoundEffect(data);
    }

    private static IEnumerator PlayMusic(string[] data)
    {
        string filepath;
        int channel;

        var parameters = ConvertDataToParameters(data);

        //try to get the name or path to the track
        parameters.TryGetValue(PARAM_SONG, out filepath);
        filepath = FilePath.GetPathToResource(FilePath.resources_music, filepath);

        //trt to get the channel for the track
        parameters.TryGetValue(PARAM_CHANNEL, out channel, defaultValue: 1);

        yield return PlayTrack(filepath, channel, parameters);
    }

    private static IEnumerator PlayAmbience(string[] data)
    {
        string filepath;
        int channel;

        var parameters = ConvertDataToParameters(data);

        //try to get the name or path to the track
        parameters.TryGetValue(PARAM_AMBIENCE, out filepath);
        filepath = FilePath.GetPathToResource(FilePath.resources_ambience, filepath);

        //trt to get the channel for the track
        parameters.TryGetValue(PARAM_CHANNEL, out channel, defaultValue: 0);

        yield return PlayTrack(filepath, channel, parameters);
    }

    private static IEnumerator PlayTrack(string filepath, int channel, CommandParameters parameters)
    {
        bool loop, immediate;
        float volumeCap, startVolume, pitch;

        //try to get the volume of the track
        parameters.TryGetValue(PARAM_VOLUME, out volumeCap, defaultValue: 1f);

        //try to get the startVolume of the track
        parameters.TryGetValue(PARAM_START_VOLUME, out startVolume, defaultValue: 0f);

        //try to get the pitch of the track
        parameters.TryGetValue(PARAM_PITCH, out pitch, defaultValue: 1f);

        //try to get if this track loops
        parameters.TryGetValue(PARAM_LOOP, out loop, defaultValue: true);

        //try to get if this an immediate trasintion
        parameters.TryGetValue(PARAM_IMMEDIATE, out immediate, defaultValue: false);

        //Run the logic
        AudioClip sound = Resources.Load<AudioClip>(filepath);

        if (sound == null)
        {
            Debug.Log($"Was not able to load track '{filepath}'");
            yield break;
        }

        yield return AudioManager.instance.PlayTrack(sound, channel, loop, startVolume, volumeCap, pitch, filepath);
    }

    private static void StopMusic(string data)
    {
        if (data == string.Empty)
            StopTrack("1");
        else
            StopTrack(data);
    }

    private static void StopAmbience(string data)
    {
        if (data == string.Empty)
            StopTrack("0");
        else
            StopTrack(data);
    }

    private static void StopTrack(string data)
    {
        if (int.TryParse(data, out int channel))
            AudioManager.instance.StopTrack(channel);
        else
            AudioManager.instance.StopTrack(data);
    }
}
