using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GraphicPanelManager : MonoBehaviour
{
    public static GraphicPanelManager instance { get; private set; }

    private const float DEFAULT_TRANSITION_SPEED = 3f;

    [SerializeField]
    private GraphicPanel[] allPanals;

    private void Awake()
    {
        instance = this;
    }

    public GraphicPanel GetPanel(string name)
    {
        name = name.ToLower();

        foreach (var panel in allPanals)
        {
            if (panel.panelName.ToLower() == name)
                return panel;
        }

        return null;
    }

    public IEnumerator TransitionScene(string nextScene)
    {
        GraphicPanel panel = GetPanel("Transition");
        GraphicLayer layer = panel.GetLayer(0, true);

        layer.SetTexture(FilePath.resources_backgroundImages + "Black", transitionSpeed: 5f);

        //AudioManager.instance.PlaySoundEffect(FilePath.resources_sfx + "Move");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene(nextScene);
    }

    public IEnumerator StartScene()
    {
        GraphicPanel black = GraphicPanelManager.instance.GetPanel("Transition");
        GraphicLayer blackLayer = black.GetLayer(0, true);

        yield return blackLayer.SetTexture(FilePath.resources_backgroundImages + "Black", immediate: true);

        yield return blackLayer.Clear(transitionspeed: 1f);
    }
}
